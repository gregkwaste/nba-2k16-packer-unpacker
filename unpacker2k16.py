# GPL LICENCE
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import struct
import sys
import os
from StringIO import StringIO
# Import Custom Modules/Libraries
sys.path.append('../../gk_blender_lib/modules')
from string_func import *

lic = '''NBA 2K16 PACKER/UNPACKER is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

NBA 2K16 PACKER/UNPACKER is distributed in the hope that it will
be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with NBA 2K16 PACKER/UNPACKER.
If not, see <http://www.gnu.org/licenses/>.'''

# f = open('I:\\GameInstalls\\NBA 2K16\\manifest', 'r')
# t = open('filelist', 'w')
# for line in f.readlines():
#     split = line.split('\t')
#     archname = split[0]
#     split[1] = split[1].lstrip()
#     name, offset, size = split[1].split(' ')
#     t.write(archname + '\t' + name + '\t' + offset + '\t' + size)
# print archname, name, int(offset), int(size)

# t.close()
# f.close()


def main():
    print('#####################################')
    print('### NBA 2K16 PACKER/UNPACKER v1.0 ###')
    print('####### Created by gregkwaste #######')
    print('#########Copyright: (c) 2015 ########')
    print('#####################################')
    try:
        opt = sys.argv[1]
    except:
        print('No input')
        return
    if opt == '-u':
        try:
            mode = sys.argv[2]
        except:
            mode = 'All'
        print('Unpacking')
        unpack(mode)
    elif opt == '-p':
        try:
            mode = sys.argv[2]
        except:
            mode = 'All'
        print('Packing')
        pack(mode)
    elif opt == '-v':
        print(lic)


def pack(mode):
    path = os.getcwd()
    print path
    activearch = None
    f = open(os.path.join(path, 'Unpack', 'unpack_list'), 'r')
    for line in f.readlines():
        split = line.split('\t')[0:-1]
        if (mode != 'All'):
            if (split[0] != mode):
                continue
        try:
            t = open(os.path.join(path, 'Unpack', split[1]), 'rb')
            filedata = t.read()
            t.close()
            filedata += bytes(b'\x00' * (int(split[3]) - len(filedata)))
        except IOError:
            print 'File Missing', split[1]
            continue

        try:
            if os.path.basename(activearch.name) != split[0]:
                activearch.close()
                activearch = open(os.path.join(path, 'UnPack', split[0]), 'wb')
        except:
            '''Init'''
            activearch = open(os.path.join(path, 'UnPack', split[0]), 'wb')
            pass
        off = int(split[2])

        activearch.seek(off)
        activearch.write(filedata)

        print split
    activearch.close()
    f.close()


def unpack(mode):
    path = os.getcwd()
    f = open(os.path.join(path, '0A'), 'rb')
    f.seek(16)
    count_0 = struct.unpack('<I', f.read(4))[0]
    f.seek(12, 1)
    count_1 = struct.unpack('<I', f.read(4))[0]
    f.seek(12, 1)
    s = 0
    print('Counts: ', count_0, count_1)
    archlist = []
    for i in range(count_0):
        size = struct.unpack('<Q', f.read(8))[0]
        f.read(8)
        name = read_string_1(f)
        f.read(13 + 16)
        # print(name,hex(size),f.tell())
        # self.main_list.append(None,(name,s))
        archlist.append((name, s, size, []))
        print(name, s, size)
        s += size

    print('Total Size: ', s)

    # Store archives data
    data = StringIO()
    data.write(f.read(0x18 * count_1))
    data.seek(0)
    for i in range(count_1):
        sa = struct.unpack('<Q', data.read(8))[0]
        id0 = struct.unpack('<I', data.read(4))[0]
        sb = struct.unpack('<I', data.read(4))[0]
        id1 = struct.unpack('<Q', data.read(8))[0]
        # f.write(id1)
        for j in range(count_0 - 1, -1, -1):
            val = archlist[j][1]  # full archive calculated offset
            if id1 >= val:
                archlist[j][3].append(
                    ['unknown_' + str(i), id1 - val, sa])
                break

    flist = open(os.path.join(path, 'Unpack', 'unpack_list'), 'w')
    for arch in archlist:
        print arch[0]
        if (mode != 'All'):
            if (arch[0] != mode):
                continue
        temp = sorted(arch[3], key=lambda s: s[1], reverse=False)
        t = open(os.path.join(path, arch[0]), 'rb')
        for subf in temp:
            name, off, size = subf
            t.seek(off)
            k = open(os.path.join(path, 'Unpack', name), 'wb')
            k.write(t.read(size))
            k.close()
            # Write to unpack_list
            flist.write(
                arch[0] + '\t' + name + '\t' + str(off) +
                '\t' + str(size) + '\t' + '\n')
        t.close()
    flist.close()

main()
